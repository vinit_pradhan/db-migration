const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const AWS = require('aws-sdk');
const db = require('./database');
const port = process.env.PORT;
const fs = require('fs');
const json2csv = require('json2csv').Parser;

app.use(bodyParser.urlencoded({ extended: true }));

// get users that need to be migrated
//db.query('select users.id, users.email, users.first_name, users.last_name, users.handle, users.bio, users.profile_image, identities.provider from users inner join identities on users.id = identities.user_id where users.id in (select user_id from stories.documents where locale="en" and editorial_status = "EDITOR_APPROVED")', (err, result) => {
//    if (err) throw err;
//    const fields = ['id', 'email', 'first_name', 'last_name', 'handle', 'bio', 'profile_image'];
//    const opts= { fields };
//    const parser = new json2csv(opts);
//    const csv = parser.parse(result);
//    fs.writeFile('users.csv', csv, (err) => {
//        if (err) throw err;
//        console.log('Users file saved');
//    });
//});

// get tags that needs to be migrated
db.query('select post_categories.id, post_categories.name, post_categories.uniq_id from stories.post_categories where id in (select followable_id from stories.follows where follower_id in (select id from stories.documents where locale = "en" and editorial_status = "EDITOR_APPROVED"))', (err, result) => {
    if (err) throw err;
    const fields = ['id', 'name', 'uniq_id'];
    const opts = { fields };
    const parser = new json2csv(opts);
    const csv = parser.parse(result);
    fs.writeFile('tags.csv', csv, (err) => {
        if (err) throw err;
        console.log('Tags file saved');
    });
});

app.listen(port, () => {
    console.log('Server Started...');
    console.log('Hello World!');
});
